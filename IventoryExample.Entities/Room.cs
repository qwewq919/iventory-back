﻿using IventoryExample.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Entities
{
    public class Room : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public Room(string name)
        {
            IsActive = true;
            Name = name;
        }
    }
}
