﻿using iventoryExample.Entities;
using IventoryExample.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Entities
{
    public class Logs : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string AccountId { get; set; }
        public int ItemId { get; set; }
        public string RecipientId { get; set; }
        public string NewRoomId { get; set; }
        public string NewCategoryId { get; set; }
        public DateTime Date { get; set; }
        public Statuses Status { get; set; }
        public bool IsActive { get; set; }


        public virtual Item Item { get; set; }
        public virtual Account Account { get; set; }
        public virtual Account Recipient { get; set; }
        public virtual Room NewRoom { get; set; }
        public virtual Category NewCategory { get; set; }

        [Flags]
        public enum Statuses
        {
            Created = 1,
            Updated = 2,
            Deleted = 4,
            Withdrawn = 8,
            Uncategorized = 16,
            Eviction = 32,
            UserChange = 64,
            RoomChange = 128,
            CategoryChange = 256,
            AddUser = 512,
            AddRoom = 1024,
            AddCategory = 2048
        }
    }
}
