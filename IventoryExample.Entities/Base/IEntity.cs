﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Entities.Base
{
    public interface IEntity<T>
    {
        public T Id { get; set; }
        public bool IsActive { get; set; }

    }

    public interface IEntity : IEntity<int> { }
}
