﻿using IventoryExample.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Entities
{
    public class Item : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AccountId { get; set; }
        public Guid? RoomId { get; set; }
        public bool IsActive { get; set; }
        public Guid? CategoryId { get; set; }
        public string ImageSrc { get; set; }

        public virtual Room Room { get; set; }
        public virtual Category Category { get; set; }
        public virtual Account Account { get; set; }

        public Item(string name)
        {
            IsActive = true;
            Name = name;
        }
        // public object Categories { get; set; }
    }

 
}
