﻿using IventoryExample.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Entities
{
    public class Category : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public ICollection<Item> Items { get; set; }

        protected Category() { }

        public Category(string name)
        {
            IsActive = true;
            Name = name;
        }
    }

}
