﻿using IventoryExample.Entities.Base;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
namespace iventoryExample.Entities
{
    public class Account : IdentityUser
    {

        //public ICollection<Item> Items { get; set; }
        //public List<Item> Items { get; set; }
        public bool IsActive { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Role Roles { get; set; }

        public Account()
        {
            IsActive = true;
            Roles = (Role)1;

        }
    }




    [Flags]
    public enum Role
    {
        User = 1,
        Admin = 2
    }


}
