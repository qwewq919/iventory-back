﻿using iventoryExample.Database;
using IventoryExample.Database.Repositories.Implementations;
using IventoryExample.Database.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Database
{
    public class ContainerConfiguration
    {
        public static IServiceCollection Configure(IServiceCollection services)
        {
            string connectionString = "Server=DESKTOP-1Q15HNT\\SQLEXPRESS;Database=inventorydb;Trusted_Connection=True;";
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(connectionString), ServiceLifetime.Singleton);
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IItemRepository, ItemRepository>();
            services.AddTransient<IRoomRepository, RoomRepository>();
            services.AddTransient<ILogsRepository, LogsRepository>();
            return services;
        }
    }
}
