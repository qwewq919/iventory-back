﻿using iventoryExample.Entities;
using IventoryExample.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Database
{
    public class ApplicationContext : IdentityDbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
     
        public DbSet<Item> Items { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public ApplicationContext()
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = "Server=DESKTOP-1Q15HNT\\SQLEXPRESS;Database=inventorydb;Trusted_Connection=True;";
            optionsBuilder.UseSqlServer(connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Item>()
            //   .HasOne(e => e.Account)
            //   .WithMany()
            //   .HasForeignKey(x => x.AccountId)
            //   .OnDelete(DeleteBehavior.ClientSetNull);

            //modelBuilder.Entity<Item>()
            //  .HasOne(e => e.Category)
            //  .WithMany(c => c.Items)
            //  .HasForeignKey(x => x.CategoryId)
            //  .OnDelete(DeleteBehavior.ClientSetNull);

        }
    }
}
