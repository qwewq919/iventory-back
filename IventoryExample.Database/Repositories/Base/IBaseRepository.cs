﻿using IventoryExample.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Database.Repositories.Base
{
    public interface IBaseRepository<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
        where TKey : struct
    {
        Task<TEntity> GetById(TKey id);
        Task<IEnumerable<TEntity>> GetAll();
        Task<IEnumerable<TEntity>> GetData(int PageIndex , int PageSize);
        TEntity Add(TEntity item);
        TEntity Update(TEntity item);
        Task SaveChangesAsync();
    }
}
