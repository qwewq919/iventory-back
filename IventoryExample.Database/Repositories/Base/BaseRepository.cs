﻿using iventoryExample.Database;
using iventoryExample.Entities;
using IventoryExample.Entities.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Database.Repositories.Base
{
    public class BaseRepository<TEntity, TKey>: IBaseRepository<TEntity, TKey>
        where TEntity: class, IEntity<TKey>
        where TKey: struct
    {
        private readonly ApplicationContext _dbcontext;
        protected readonly DbSet<TEntity> _table;

        public BaseRepository(ApplicationContext dbContext)
        {
            _dbcontext = dbContext;
            _table = _dbcontext.Set<TEntity>();
        }

        public async Task<TEntity> GetById(TKey id)
        {
            return await _table
                .FirstOrDefaultAsync(x => x.Id.Equals(id));
        }


        public async  Task <IEnumerable<TEntity>> GetAll()
        {
            return await _table
                .ToListAsync();
        }


        public TEntity Add(TEntity item)
        {
            var res = _table.Add(item);
            return res.Entity;
        }

        public TEntity Update(TEntity item)
        {
            var res = _table.Update(item);
            return res.Entity;
        }

        public async Task<IEnumerable<TEntity>> GetData(int PageIndex, int PageSize)
        {
            return await _table
                .Where(x => x.IsActive)
                .Skip(PageIndex * PageSize)
                .Take(PageSize)
                .ToListAsync();
        }

        //private Account AuthentucateUser(Account item)
        //{
        //   var  account = _table.SingleOrDefault(u => u.Email == item.Email && u.Password == item.Password);
        //    return account;
        //}


        public async Task SaveChangesAsync()
            => await _dbcontext.SaveChangesAsync();

    }
}
