﻿using iventoryExample.Database;
using iventoryExample.Entities;
using IventoryExample.Database.Repositories.Base;
using IventoryExample.Database.Repositories.Interfaces;
using IventoryExample.Entities.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Database.Repositories.Implementations
{
    public class CategoryRepository : BaseRepository<Category, Guid>, ICategoryRepository
    {
        private readonly ApplicationContext _context;
        public CategoryRepository(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            return await _table
             .Where(x => x.IsActive)
             .ToListAsync();
        }
    }
}
