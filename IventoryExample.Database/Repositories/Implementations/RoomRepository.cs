﻿using iventoryExample.Database;
using iventoryExample.Entities;
using IventoryExample.Database.Repositories.Base;
using IventoryExample.Database.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Database.Repositories.Implementations
{
    public class RoomRepository : BaseRepository<Room, Guid>, IRoomRepository
    {
        private readonly ApplicationContext _context;
        public RoomRepository(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Room>> GetAllActive()
        {
            return await _table
                .Where(x => x.IsActive)
                .ToListAsync();
        }
    }
}
