﻿using iventoryExample.Database;
using iventoryExample.Entities;
using IventoryExample.Database.Repositories.Base;
using IventoryExample.Database.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Database.Repositories.Implementations
{
    
    public class ItemRepository : BaseRepository<Item ,int>, IItemRepository
    {
        private readonly ApplicationContext _context;
        public ItemRepository(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Item>> GetAllWithRooms()
        {
            return await _table
              .Include(x => x.Room)
              .Include(x => x.Category)
              .Include(x => x.Account)
              .Where(x => x.IsActive)
              .ToListAsync();
        }
    }
}
