﻿using iventoryExample.Database;
using IventoryExample.Database.Repositories.Base;
using IventoryExample.Database.Repositories.Interfaces;
using IventoryExample.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Database.Repositories.Implementations
{
    public class LogsRepository : BaseRepository<Logs, Guid>, ILogsRepository
    {
        private readonly ApplicationContext _context;
        public LogsRepository(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Logs>> GetLogsAsync()
        {
            return await _table
              .Include(x => x.Item)
              .Include(x => x.Account)
              .Include(x => x.Recipient)
              .Include(x => x.NewRoom)
              .Include(x => x.NewCategory)
              .ToListAsync();
        }
        public async Task AddRange(List<Logs> item)
        {
            await _table.AddRangeAsync(item);
        }

    }
}
