﻿using iventoryExample.Entities;
using IventoryExample.Database.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Database.Repositories.Interfaces
{
    public interface ICategoryRepository : IBaseRepository<Category, Guid>
    {
        Task<IEnumerable<Category>> GetAllAsync();
    }
}
