﻿using IventoryExample.Database.Repositories.Base;
using IventoryExample.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Database.Repositories.Interfaces
{
    public interface ILogsRepository : IBaseRepository<Logs, Guid>
    {
        Task<IEnumerable<Logs>> GetLogsAsync();
        Task AddRange(List<Logs> item);
    }
}
