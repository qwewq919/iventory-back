﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Api
{
    public class ContainerConfiguration
    {
        public static IServiceCollection Configure(IServiceCollection services)
        {
            // сюда можешь свой маппер добавить
            // отдельный, для уровня апишки
            // это задача на самостоятельное выполнение будет

            // пока тут зависимостей не вижу
            // но добавятся если конфигурации базовые повыносить захочешь и т.д.
            IventoryExample.Services.ContainerConfiguration.Configure(services);

            return services;
        }
    }
}
