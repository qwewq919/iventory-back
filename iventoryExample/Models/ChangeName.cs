﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Api.Models
{
    public class ChangeName
    {
        public string id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
