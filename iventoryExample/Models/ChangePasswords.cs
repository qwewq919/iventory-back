﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Api.Models
{
    public class ChangePasswords
    {
        // string id, string password, string newPassword
        public string id { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
    }
}
