﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public  Account Account { get; set; }
        public Guid? AccountId { get; set; }
        public Room Room { get; set; }
        public Guid? RoomId { get; set; }
        public bool IsActive { get; set; }
        public Category Category { get; set; }
        public Guid? CategoryId { get; set; }
    }

 
}
