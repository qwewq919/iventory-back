﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Models
{
    public class sortItem
    {
        public string category { get; set; }
        public string itemName { get; set; }
        public string room { get; set; }
        public string user { get; set; }
    }
}
