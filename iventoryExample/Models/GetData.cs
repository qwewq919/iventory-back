﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Api.Models
{
    public class GetData
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}
