﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iventoryExample.Models
{
    public class Account
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }

        //public Role Roles { get; set; }
        //public bool IsActive { get; set; }
        //public ICollection<Item> Items { get; set; }
        // public List<Item> Items { get; set; }
    }

    [Flags]
    public enum Role
    {
        User = 1,
        Admin = 2
    }

}
