﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using iventoryExample.Models;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Category;
using IventoryExample.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace iventoryExample.Controllers
{
    [Route("api/category")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        
        private Guid UserId => Guid.Parse(User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value);

        //public CategoryController(IOptions<AuthOptions> authOptions, ApplicationContext context)
        //{
        //    db = context;
        //    this.authOptions = authOptions;
        //}

        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        [HttpGet("getItem/{id}")]
        public async Task<GetCategoryDto> GetById([FromRoute]Guid id)
        {
            var category = await _categoryService.GetById(id);
            return category;
        }
        

        [HttpGet]
        //[Authorize(Roles = "Admin")]
        [Route("")]
        public async Task<IActionResult> GetAllItems()
        {
            var category = await _categoryService.GetAll();
    
            return Ok(category);
        }

        [HttpPost]
        [Route("Update")]
  //      [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Update(UpdateCategoryDto category)
        {
            await _categoryService.Update(category);
            return Ok();
        }

        [HttpPost]
        [Route("add")]
        //       [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Add(CreateCategoryDto category)
        {
            await _categoryService.Add(category);
            return Ok();
        }


        [HttpGet("deactivate/{id}")]
        public async Task<IActionResult> deactivate([FromRoute] Guid id)
        {
            await _categoryService.DeActivate(id);
            return Ok();
        }

        [HttpGet("activate/{id}")]
        public async Task<IActionResult> Activate([FromRoute] Guid id)
        {
            await _categoryService.Activate(id);
            return Ok();
        }

        [Route("getData")]
        [HttpPost]
        public async Task<IActionResult> GetData([FromBody] GetDataDto getData)
        {
            var res = await _categoryService.GetData(getData);
            return Ok(res);
        }

        [Route("getListSize")]
        [HttpGet]
        public async Task<IActionResult> getListSize()
        {
            var res = await _categoryService.GetLenghData();
            return Ok(res);
        }

    }
}
