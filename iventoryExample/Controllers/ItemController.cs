﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iventoryExample.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using IventoryExample.Services.Interfaces;
using IventoryExample.Services.Dtos.Item;
using IventoryExample.Services.Dtos;
using QRCoder;
using System.Drawing;
using System.IO;
using System.Net.Http.Headers;
using iventoryExample.Api.Models;
using System.Collections.Generic;

namespace Resourse.Controllers
{
    [Route("api/item")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private Guid UserId => Guid.Parse(User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier ).Value);

        //      public ItemController(IOptions<AuthOptions> authOptions, ApplicationContext context)
        //    {
        //      db = context;
        //    this.authOptions = authOptions;
        //}
        private readonly IItemService _itemService;
        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }






        [HttpGet("{id}")]
        public async Task<GetItemDto> GetById([FromRoute] int id)
        {
            var item = await _itemService.GetById(id);
            return item;
        }


     //   [HttpGet]
     ////   [Authorize(Roles = "Admin")]
     //   [Route("")]
     //   public async Task<IActionResult> GetAllItems()
     //   {
     //       var item = await _itemService.GetAllWithRooms();
     //       //var allItems = await category.Where(b => b.IsActive).ToListAsync();
     //       return Ok(item);
     //   }


        [HttpPost]
        [Route("add")]
    //    [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Add(CreateItemDto item)
        {
            var a = HttpContext.Request.Headers.ToList();
            var tokenObj = a
                .Where(x => x.Key == "Authorization")
                .Select(p => new { Key = p.Key, Value = p.Value })
                .FirstOrDefault();
            if (tokenObj == null)
            {
                return null;
            }
            var token = tokenObj.Value;
            var tokenStr = tokenObj.Value.ToString();
            var tokenProcessed = tokenStr.Replace("Bearer", "").Trim();
            await _itemService.Add(item, tokenProcessed);
            return Ok(new
            {
                res = "oks"
            });
        }

        [HttpPost]
        [Route("update")]
    //    [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Update(UpdateItemDto item  )
        {
            var a = HttpContext.Request.Headers.ToList();
            var tokenObj = a
                .Where(x => x.Key == "Authorization")
                .Select(p => new { Key = p.Key, Value = p.Value })
                .FirstOrDefault();
            if (tokenObj == null)
            {
                return null;
            }
            var token = tokenObj.Value;
            var tokenStr = tokenObj.Value.ToString();
            var tokenProcessed = tokenStr.Replace("Bearer", "").Trim();
            await _itemService.Update(item, tokenProcessed);
            return Ok(new { res = "ok"});
        }


        [Route("deactivate")]
     //   [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> deactivate([FromBody]int id)
        {
            var a = HttpContext.Request.Headers.ToList();
            var tokenObj = a
                .Where(x => x.Key == "Authorization")
                .Select(p => new { Key = p.Key, Value = p.Value })
                .FirstOrDefault();
            if (tokenObj == null)
            {
                return null;
            }
            var token = tokenObj.Value;
            var tokenStr = tokenObj.Value.ToString();
            var tokenProcessed = tokenStr.Replace("Bearer", "").Trim();
            await _itemService.DeActivate(id, tokenProcessed);
            return Ok();
        }

        [Route("activate")]
    //    [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Activate([FromBody] int id)
        {
            await _itemService.Activate(id);
            return Ok();
        }

        [Route("sort")]
    //    [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> sort([FromBody] SortItemDto item)
        {
            var query = await _itemService.sort(item);
            return Ok(query);
        }

        [Route("getData")]
        //[Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> GetData([FromBody] GetDataDto getData)
        {
            var res = await _itemService.GetData(getData);
            return Ok(res);
        }

        [Route("getListSize")]
   //     [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> getListSize()
        {
            var res = await _itemService.GetLenghData();
            return Ok(res);
        }

        [Route("getQR/{id}")]
        [HttpGet]
        public IActionResult Index([FromRoute]string id)
        {
            var res = _itemService.GetQR(id);
            return Ok(res);
        }
        [Route("Upload/{id}")]
        [HttpPost, DisableRequestSizeLimit]
        public async Task<IActionResult> Uploads([FromRoute]int id)
        {
            var file =  Request.Form.Files[0];
            var res = await _itemService.Upload(file, id);
             return Ok(new Res() { result = res });
        }
        [Route("GetImg/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetImg([FromRoute] int id)
        {
     
                var res = await _itemService.GetImgPath(id);
                return Ok(res);
           
        }

        [Route("getPdf")]
        //    [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> toPdf([FromBody] ArrayDtoPayload items)
        {
            var query =  _itemService.ToPdf(items.items);
            return Ok(query);
        }

        [Route("userDelet")]
        //    [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> DeletUser([FromBody] int id)
        {

            var a = HttpContext.Request.Headers.ToList();
            var tokenObj = a
                .Where(x => x.Key == "Authorization")
                .Select(p => new { Key = p.Key, Value = p.Value })
                .FirstOrDefault();
            if (tokenObj == null)
            {
                return null;
            }
            var token = tokenObj.Value;
            var tokenStr = tokenObj.Value.ToString();
            var tokenProcessed = tokenStr.Replace("Bearer", "").Trim();
            var query = _itemService.DeletUser(id , tokenProcessed);
            return Ok();
        }


        [Route("DeletCategorie")]
        //    [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> DeletCategorie([FromBody] int id)
        {

            var a = HttpContext.Request.Headers.ToList();
            var tokenObj = a
                .Where(x => x.Key == "Authorization")
                .Select(p => new { Key = p.Key, Value = p.Value })
                .FirstOrDefault();
            if (tokenObj == null)
            {
                return null;
            }
            var token = tokenObj.Value;
            var tokenStr = tokenObj.Value.ToString();
            var tokenProcessed = tokenStr.Replace("Bearer", "").Trim();
            var query = _itemService.DeletCategorie(id, tokenProcessed);
            return Ok();
        }

        [Route("DeletRoom")]
        //    [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> DeletRoom([FromBody] int id)
        {

            var a = HttpContext.Request.Headers.ToList();
            var tokenObj = a
                .Where(x => x.Key == "Authorization")
                .Select(p => new { Key = p.Key, Value = p.Value })
                .FirstOrDefault();
            if (tokenObj == null)
            {
                return null;
            }
            var token = tokenObj.Value;
            var tokenStr = tokenObj.Value.ToString();
            var tokenProcessed = tokenStr.Replace("Bearer", "").Trim();
            var query = _itemService.DeletRoom(id, tokenProcessed);
            return Ok();
        }



    }

}
