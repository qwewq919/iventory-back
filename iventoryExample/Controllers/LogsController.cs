﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Logs;
using IventoryExample.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iventoryExample.Api.Controllers
{
    [Route("api/logs")]
    [ApiController]
    public class LogsController : ControllerBase
    {
        private Guid UserId => Guid.Parse(User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value);

        private readonly ILogsService _logsService;
        public LogsController(ILogsService logsService)
        {
            _logsService = logsService;
        }

        [HttpGet]
      //  [Authorize(Roles = "Admin")]
        [Route("")]
        public async Task<IActionResult> GetAllItems()
        {
            
            var logs = await _logsService.GetAll();

            return Ok(logs);
        }


        [Route("getListSize")]
    //    [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> getListSize()
        {
            var res = await _logsService.GetLenghData();
            return Ok(res);
        }

        [Route("getData")]
        //[Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> GetData([FromBody] GetDataDto getData)
        {
            var a = HttpContext.Request.Headers.ToList();
            var res = await _logsService.GetData(getData);
            return Ok(res);
        }

        [Route("sort")]
        //    [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> sort([FromBody] SortLogsDto item)
        {
            var query = await _logsService.sort(item);
            return Ok(query);
        }
    }
}
