﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iventoryExample.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using IventoryExample.Services.Interfaces;
using IventoryExample.Services.Dtos.Account;
using iventoryExample.Api.Models;
using System.Collections.Generic;
using IventoryExample.Services.Dtos;
using iventoryExample.Entities;

namespace iventoryExample.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : Controller
    {

         private Guid UserId => Guid.Parse(User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value);
         
        // private string UserId => User?.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;


        //      public ItemController(IOptions<AuthOptions> authOptions, ApplicationContext context)
        //    {
        //      db = context;
        //    this.authOptions = authOptions;
        //}
        private readonly IAcccountService _acccountService;
        public UserController(IAcccountService acccountService)
        {
            _acccountService = acccountService;
        }


        [Route("{id}")]
        [HttpGet]
        public async Task<IActionResult> GetUser([FromRoute]string id)
        {
            var res = await _acccountService.GetUser(id);
            return Ok(res);
        }

        [Route("person")]

        [HttpGet]
        public async Task<IActionResult> GetUserForPerson()
        {

            var a = HttpContext.Request.Headers.ToList();
            var tokenObj = a
                .Where(x => x.Key == "Authorization")
                .Select(p => new { Key = p.Key, Value = p.Value })
                .FirstOrDefault();
            if (tokenObj == null)
            {
                return null;
            }
            var token = tokenObj.Value;
            var tokenStr = tokenObj.Value.ToString();
            var tokenProcessed = tokenStr.Replace("Bearer", "").Trim();

           // var id = UserId.ToString();
            var res = await _acccountService.GetUserOfToken(tokenProcessed);
            return Ok(res);
        }

        [HttpGet]
        //[Authorize(Roles = "Admin")]
        [Route("")]
        public async Task<IActionResult> GetAllItems()
        {
            var users = await _acccountService.GetAll();
            return Ok(users);
        }


        [Route("deactivate/{id}")]
        //   [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> DeActivate([FromRoute]string id)
        {
            await _acccountService.DeActivate(id);
            return Ok();
        }



        [Route("activate/{id}")]
        //   [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Activate([FromRoute] string id)
        {
            await _acccountService.DeActivate(id);
            return Ok();
        }

        [Route("roleChange")]
        //   [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> RoleChange([FromBody] Roles role)
        {
            await _acccountService.RoleChange(role.id , role.role);
            return Ok();
        }

        [Route("changePassword")]
        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswords changePasswords)
        {
            var res = await _acccountService.ChangePassword(changePasswords.id, changePasswords.password, changePasswords.newPassword);
            return Ok(res);
        }

        [Route("changeName")]
        [HttpPost]
        public async Task<IActionResult> ChangeName([FromBody] ChangeName changeName)
        {
            var res = await _acccountService.ChangeName(changeName.id, changeName.FirstName, changeName.LastName);
            return Ok(res);
        }

        [Route("getData")]
        [HttpPost]
        public async Task<IActionResult> GetData([FromBody] GetDataDto getData)
        {
            var res = await _acccountService.GetData(getData);
            return Ok(res);
        }

        [Route("getListSize")]
        [HttpGet]
        public async Task<IActionResult> getListSize()
        {
            var res = await _acccountService.GetLenghData();
            return Ok(res);
        }

        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> EditUser([FromBody] Entities.Account account)
        {
            var res = await _acccountService.EditUser(account);
            return Ok(res);
        }
    }
}
