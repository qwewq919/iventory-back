﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using iventoryExample.Models;
using IventoryExample.Common;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Room;
using IventoryExample.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace iventoryExample.Controllers
{
    [Route("api/rooms")]
    [ApiController]
    public class RoomsController : ControllerBase
    {
        private IOptions<AuthOptions> authOptions;
        
        private Guid UserId => Guid.Parse(User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value);


        private readonly IRoomService _roomService;
        public RoomsController(IRoomService roomService)
        {
            _roomService = roomService;
        }


        [HttpGet("getItem/{id}")]
        public async Task<GetRoomDto> GetById([FromRoute] Guid id)
        {
            var room = await _roomService.GetById(id);
            return room;
        }


        [HttpGet]
        //   [Authorize(Roles = "Admin")]
        [Route("")]
        public async Task<IActionResult> GetAllItems()
        {
            var room = await _roomService.GetAll();
            //var allItems = await room.Where(b => b.IsActivate).ToListAsync();
            return Ok(room);
        }

        [HttpPost]
        [Route("update")]
        //    [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Update(UpdateRoomDto room)
        {
            await _roomService.Update(room);
            return Ok();
        }

        [HttpPost]
        [Route("add")]
        //   [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Add(CreateRoomDto room)
        {
            await _roomService.Add(room);
            return Ok();
        }


        //[Route("deactivate")]
        //    [Authorize(Roles = "Admin")]
        //[HttpPost]
        [HttpGet("deactivate/{id}")]
        public async Task<IActionResult> deactivate([FromRoute] Guid id)
        {
            await _roomService.DeActivate(id);
            return Ok();
        }

        [HttpGet("activate/{id}")]
        public async Task<IActionResult> Activate([FromRoute] Guid id)
        {
            await _roomService.Activate(id);
            return Ok();
        }

        [Route("getData")]
        [HttpPost]
        public async Task<IActionResult> GetData([FromBody] GetDataDto getData)
        {
            var res = await _roomService.GetData(getData);
            return Ok(res);
        }

        [Route("getListSize")]
        [HttpGet]
        public async Task<IActionResult> getListSize()
        {
            var res = await _roomService.GetLenghData();
            return Ok(res);
        }

    }
}
