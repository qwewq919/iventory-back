﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using iventoryExample.Models;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Microsoft.PowerBI.Api;
using IventoryExample.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;

namespace iventoryExample.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private string Email => User.Claims.Single(c => c.Type == ClaimTypes.Email).Value;
        //private Guid UserId => Guid.Parse(User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value);
        private readonly IAcccountService _acccountService;
        public AuthController(IAcccountService acccountService)
        {
            _acccountService = acccountService;
        }


        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] Login request)
        {
              var res = await _acccountService.Register(request.Email, request.Password , request.FirstName , request.LastName);
              if (res.success)
                {
                    return Ok(res.payload);

                }

            return BadRequest(res.payload);

        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] Login request)
        {
            var res = await _acccountService.Login(request.Email, request.Password);
            if (res.success)
            {
                return Ok(res.payload);

            }
   
            return BadRequest(res.payload);
        }

        



    }
}
