﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos
{
    public class GetDataDto
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}
