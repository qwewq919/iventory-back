﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Room
{
    public class CreateRoomDto
    {
        public string Name { get; set; }
    }
}
