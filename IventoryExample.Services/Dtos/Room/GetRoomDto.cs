﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Room
{
    public class GetRoomDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
