﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IventoryExample.Services.Dtos.Item
{
    public class ArrayDtoPayload
    {
        public IEnumerable<int> items { get; set; }
    }
}
