﻿using IventoryExample.Services.Dtos.Account;
using IventoryExample.Services.Dtos.Category;
using IventoryExample.Services.Dtos.Room;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Item
{
    public class GetItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageSrc { get; set; }

        public virtual GetRoomDto Room { get; set; }
        public virtual GetCategoryDto Category { get; set; }
        public virtual GetAccountDto  Account { get; set; }

    }
}
