﻿using iventoryExample.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Item
{
    public class UpdateItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActivate { get; set; }
        public string AccountId { get; set; }
        public string CategoryId { get; set; }
        public string RoomId { get; set; }
        public string ImageSrc { get; set; }
    }
}
