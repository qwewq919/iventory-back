﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Item
{
    public class CreateItemDto
    {
        public string Name { get; set; }
    }
}
