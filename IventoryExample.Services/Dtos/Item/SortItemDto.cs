﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Item
{
    public class SortItemDto
    {
        public string category { get; set; }
        public string itemName { get; set; }
        public string room { get; set; }
        public string user { get; set; }
    }
}
