﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Account
{
    public class LoginMessagePayload
    {
        
    }
    public class LoginMessageSuccess : LoginMessagePayload
    {
        public string access_token { get; set; }
    }
    public class LoginMessageError : LoginMessagePayload
    {
        public string message { get; set; }
    }
    public class LoginResponseDto
    {
        public bool success { get; set; }
        public LoginMessagePayload payload { get; set; }
    }


}
