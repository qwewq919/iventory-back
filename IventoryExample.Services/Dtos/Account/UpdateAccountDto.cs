﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Account
{
    public class UpdateAccountDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public bool IsActivate { get; set; }
        public Role Roles { get; set; }

        [Flags]
        public enum Role
        {
            User = 1,
            Admin = 2
        }
    }
}
