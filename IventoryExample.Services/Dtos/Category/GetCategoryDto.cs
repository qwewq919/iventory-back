﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Category
{
    public class GetCategoryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
