﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Category
{
    public class CreateCategoryDto
    {
        public string Name { get; set; }
    }
}
