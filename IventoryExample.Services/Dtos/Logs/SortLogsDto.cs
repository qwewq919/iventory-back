﻿using IventoryExample.Services.Dtos.Account;
using IventoryExample.Services.Dtos.Item;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Logs
{
    public class SortLogsDto
    {
        public string items { get; set; }
        public string user { get; set; }
        public string Status { get; set; }

    }
}
