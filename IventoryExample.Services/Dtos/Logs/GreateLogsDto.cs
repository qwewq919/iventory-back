﻿using IventoryExample.Services.Dtos.Account;
using IventoryExample.Services.Dtos.Category;
using IventoryExample.Services.Dtos.Item;
using IventoryExample.Services.Dtos.Room;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Dtos.Logs
{
    public class GreateLogsDto
    {
        public Guid Id { get; set; }
        public string AccountId { get; set; }
        public int ItemId { get; set; }
        public DateTime Date { get; set; }
        public Statuses Status { get; set; }


        public virtual GetAccountDto Recipient { get; set; }
        public virtual GetItemDto Item { get; set; }
        public virtual GetAccountDto Account { get; set; }
        public virtual GetRoomDto NewRoom { get; set; }
        public virtual GetCategoryDto NewCategory { get; set; }


        [Flags]
        public enum Statuses
        {
            Created = 1,
            Updated = 2,
            Deleted = 4,
            Withdrawn = 8,
            Uncategorized = 16,
            Eviction = 32,
            UserChange = 64,
            RoomChange = 128,
            CategoryChange = 256,
            AddUser = 512,
            AddRoom = 1024,
            AddCategory = 2048
        }
    }
}
