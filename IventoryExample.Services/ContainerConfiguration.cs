﻿using AutoMapper;
using IventoryExample.Services.Implementations;
using IventoryExample.Services.Interfaces;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services
{
    public class ContainerConfiguration
    {
        public static IServiceCollection Configure(IServiceCollection services)
        {
            services.Configure<FormOptions>(o => {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });


            services.AddAutoMapper(typeof(IMarker));
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IItemService, ItemService>();
            services.AddScoped<IRoomService, RoomService>();
            services.AddScoped<IAcccountService, AccountService>();
            services.AddScoped<ILogsService, LogsService>();
            services.AddIdentity<iventoryExample.Entities.Account, Microsoft.AspNetCore.Identity.IdentityRole>(
                  options =>
                            options.Password = new PasswordOptions
                            {
                                RequireDigit = false,
                                RequiredLength = 6,
                                RequireLowercase = false,
                                RequireUppercase = false,
                                RequireNonAlphanumeric = false
                            })
                        .AddEntityFrameworkStores<iventoryExample.Database.ApplicationContext>();
                  

            Database.ContainerConfiguration.Configure(services);

            return services;
        }
        
    }
}
