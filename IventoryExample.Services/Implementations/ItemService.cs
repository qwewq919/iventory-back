﻿using AutoMapper;
using iventoryExample.Entities;
using IventoryExample.Database.Repositories.Interfaces;
using IventoryExample.Entities;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Item;
using IventoryExample.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Org.BouncyCastle.Math.EC.Rfc7748;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace IventoryExample.Services.Implementations
{
    public class ItemService : IItemService
    {
        private readonly ILogsRepository _logsRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ItemService(
            IItemRepository itemRepository,
            ILogsRepository logsRepository,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor)
        {
            _logsRepository = logsRepository;
            _itemRepository = itemRepository;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task Add(CreateItemDto item , string tokenProcessed)
        {
            var entity = new Item(item.Name);
            var itemId =  _itemRepository.Add(entity);
            await _itemRepository.SaveChangesAsync();

            var id = TokenRes(tokenProcessed);

            var log = new Logs() { AccountId = id, ItemId = itemId.Id, Status = Logs.Statuses.Created, Date = DateTime.Now };
            _logsRepository.Add(log);
            await _logsRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<GetItemDto>> GetAll()
        {
            var data = await _itemRepository.GetAll();
            var res = _mapper.Map<IEnumerable<GetItemDto>>(data);
            return (res);
        }

        public async Task<IEnumerable<GetItemDto>> GetAllWithRooms()
        {
            var data = await _itemRepository.GetAllWithRooms();
            var res = _mapper.Map<IEnumerable<GetItemDto>>(data);
            return (res);
        }

        public async Task<GetItemDto> GetById(int id)
        {
            var data = await _itemRepository.GetById(id);
            return _mapper.Map<GetItemDto>(data);
        }

        public async Task Update(UpdateItemDto item, string tokenProcessed)
        {
            
            var loguArr = new List<Logs>();
            var oldItem = await _itemRepository.GetById(item.Id);
            var idUser = TokenRes(tokenProcessed);

       //    var oldItemDto = _mapper.Map<UpdateItemDto>(oldItem);

            //    var pr = new List<string>() { 'name', 'email' };
            //if (oldItem == null)
            //{
            //    throw new Exception($"Item with id {item.Id} was not found!");
            //}


            if (oldItem.AccountId == null && item.AccountId != oldItem.AccountId)
            {
                loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.AddUser, Date = DateTime.Now, RecipientId = item.AccountId });

            }
            else if (oldItem.AccountId != null && item.AccountId != oldItem.AccountId)
            {
                loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.UserChange, Date = DateTime.Now, RecipientId = item.AccountId });

            }
            else if (item.AccountId == null && item.AccountId != oldItem.AccountId)
            {
                loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.Withdrawn, Date = DateTime.Now, RecipientId = null });
            }
            else { }









            if (string.IsNullOrEmpty(item.CategoryId))
            {
                if (oldItem.CategoryId != null)
                {
                    loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.Uncategorized, Date = DateTime.Now, RecipientId = null });
                }
                else { }
            }
            else if (oldItem.CategoryId == null)
            {
                loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.AddCategory, Date = DateTime.Now, RecipientId = item.AccountId });

            }
            else if (item.CategoryId.ToString() != oldItem.CategoryId.ToString())
            {
                loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.CategoryChange, Date = DateTime.Now, RecipientId = item.AccountId });

            }
            else { }





            if (string.IsNullOrEmpty(item.RoomId))
            {
                if (oldItem.RoomId != null)
                {
                    loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.Eviction, Date = DateTime.Now, RecipientId = null });
                }
                else { }
            }
            else if (oldItem.RoomId == null )
            {
                loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.AddRoom, Date = DateTime.Now, RecipientId = item.AccountId });

            }
            else if (item.RoomId.ToString() != oldItem.RoomId.ToString())
            {
                loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.RoomChange, Date = DateTime.Now, RecipientId = item.AccountId });

            }
            else { }





            if (item.Name != oldItem.Name)
            {
                loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.Updated, Date = DateTime.Now });
            }



            //if(item.Name != oldItemDto.Name)
            //    loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.Updated, Date = DateTime.Now });

            //if(item.RoomId != oldItemDto.RoomId)
            //    loguArr.Add(new Logs() { AccountId = idUser, ItemId = item.Id, Status = Logs.Statuses.AddRoom, Date = DateTime.Now, RecipientId = item.AccountId });



            _mapper.Map(item, oldItem);
            _itemRepository.Update(oldItem);
            await _itemRepository.SaveChangesAsync();

            await _logsRepository.AddRange(loguArr);
            await _logsRepository.SaveChangesAsync();

        }

        public async Task Activate(int id)
        {
            var entity = await _itemRepository.GetById(id);
            entity.IsActive = true;
            _itemRepository.Update(entity);
            await _itemRepository.SaveChangesAsync();
        }


        public async Task DeActivate(int id , string tokenProcessed)
        {
            var entity = await _itemRepository.GetById(id);
            entity.IsActive = false;
            _itemRepository.Update(entity);
            await _itemRepository.SaveChangesAsync();

            var idUser = TokenRes(tokenProcessed);

            var log = new Logs() { AccountId = idUser, ItemId = id, Status = Logs.Statuses.Deleted, Date = DateTime.Now };
            _logsRepository.Add(log);
            await _logsRepository.SaveChangesAsync();

        }

        public async Task<IEnumerable<GetItemDto>> GetData(GetDataDto getDataDto)
        {
            var items = await _itemRepository.GetAllWithRooms();

            var allData = items
                             .Where(x => x.IsActive)
                             .Skip(getDataDto.PageIndex * getDataDto.PageSize)
                             .Take(getDataDto.PageSize);
                          
            return _mapper.Map<IEnumerable<GetItemDto>>(allData);
        }


        public async Task<int> GetLenghData()
        {
            var items = await _itemRepository.GetAll();
            var size = items.Where(x=> x.IsActive).Count();
            return size;
        }

        public byte[] GetQR(string txtQRCode)
        {
            var protocol = _httpContextAccessor.HttpContext.Request.Scheme;
            var host = _httpContextAccessor.HttpContext.Request.Host.Value.Split(":")[0];
            var resutlt = $"{protocol}://{host}:4200/qr/{txtQRCode}";
            QRCodeGenerator _qrCode = new QRCodeGenerator();
            QRCodeData _qrCodeData = _qrCode.CreateQrCode(resutlt, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(_qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            return BitmapToBytesCode(qrCodeImage);
        }

        private static Byte[] BitmapToBytesCode(Bitmap image)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public async Task<string> Upload(IFormFile file , int id)
        {
            try
            {
               // var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0)
                {
                    var TempfileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fileName =  Guid.NewGuid() + Path.GetExtension(TempfileName);
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    await UpdateSrcItem(id, dbPath);
                    return  "ok" ;
                }
                else
                {
                    return "error";
                }
            }
            catch (Exception ex)
            {
                return  $"Internal server error: {ex}";
            }
        }

        public async Task UpdateSrcItem(int id , string src)
        {
            var entity = await _itemRepository.GetById(id);

            if (entity == null)
            {
                throw new Exception($"Item with id {id} was not found!");
            }

            entity.ImageSrc = src;

            _itemRepository.Update(entity);
            await _itemRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<GetItemDto>> sort(SortItemDto item)
        {
            var query = await GetAllWithRooms();



            if (!string.IsNullOrWhiteSpace(item.itemName))
            {
                query = query.Where(x => x.Name == item.itemName);
            }
            if (!string.IsNullOrWhiteSpace(item.user))
            {
                query = query.Where(x => x.Account?.Id.ToString() == item.user);
            }
            if (!string.IsNullOrWhiteSpace(item.room))
            {
                query = query.Where(x => x.Room?.Id.ToString() == item.room);
            }
            if (!string.IsNullOrWhiteSpace(item.category))
            {
                query = query.Where(x => x.Category?.Id.ToString() == item.category);
            }


            var result = query;
            return result;
        }

        public async Task<ImageStringDto> GetImgPath(int id)
        {
            var item = await GetById(id);
            var path = item.ImageSrc;
            byte[] b = System.IO.File.ReadAllBytes(path);
             var res = "data:image/png;base64," + Convert.ToBase64String(b);
            return new ImageStringDto() { ImageString = res};
        }

        public  List<byte[]> ToPdf(IEnumerable<int> items)
        {
            List<byte[]> list = new List<byte[]>();
            foreach (var itemId in items)
            {
                
                var qr = GetQR(Convert.ToString(itemId));
                list.Add(qr);
            }
            return list;
        }



        public string TokenRes(string jwtToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadToken(jwtToken) as JwtSecurityToken;
            var tokenClaimsIdentity = new ClaimsIdentity(token.Claims);
            Lookup<string, string> claimsLookup = (Lookup<string, string>)tokenClaimsIdentity.Claims.ToLookup(p => p.Type, p => p.Value);
            string userId = "";
            foreach (var lookupItem in claimsLookup)
            {
                var key = lookupItem.Key;
                if (key == "sub")
                {
                    userId = lookupItem.FirstOrDefault();
                    return userId;
                }
            }
            return null;
        }

        public async Task DeletUser(int id , string token)
        {
            var entity = await _itemRepository.GetById(id);

            if (entity == null)
            {
                throw new Exception($"Item with id {id} was not found!");
            }

            entity.AccountId = null;

            _itemRepository.Update(entity);
            await _itemRepository.SaveChangesAsync();

            var idUser = TokenRes(token);

            var log = new Logs() { AccountId = idUser, ItemId = id, Status = Logs.Statuses.Withdrawn, Date = DateTime.Now };
            _logsRepository.Add(log);
            await _logsRepository.SaveChangesAsync();
        }

        public async Task DeletCategorie(int id, string token)
        {
            var entity = await _itemRepository.GetById(id);

            if (entity == null)
            {
                throw new Exception($"Item with id {id} was not found!");
            }

            entity.CategoryId = null;

            _itemRepository.Update(entity);
            await _itemRepository.SaveChangesAsync();

            var idUser = TokenRes(token);

            var log = new Logs() { AccountId = idUser, ItemId = id, Status = Logs.Statuses.Uncategorized, Date = DateTime.Now };
            _logsRepository.Add(log);
            await _logsRepository.SaveChangesAsync();
        }

        public async Task DeletRoom(int id, string token)
        {
            var entity = await _itemRepository.GetById(id);

            if (entity == null)
            {
                throw new Exception($"Item with id {id} was not found!");
            }

            entity.RoomId = null;

            _itemRepository.Update(entity);
            await _itemRepository.SaveChangesAsync();

            var idUser = TokenRes(token);

            var log = new Logs() { AccountId = idUser, ItemId = id, Status = Logs.Statuses.Eviction, Date = DateTime.Now };
            _logsRepository.Add(log);
            await _logsRepository.SaveChangesAsync();
            
        }

    }

}
