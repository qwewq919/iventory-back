﻿using AutoMapper;
using iventoryExample.Entities;
using IventoryExample.Common;
using IventoryExample.Database.Repositories.Interfaces;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Account;
using IventoryExample.Services.Dtos.Auth;
using IventoryExample.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Services.Implementations
{
    public class AccountService : IAcccountService
    {

        private readonly AuthOptions _options;
        //   private readonly AuthOptions _options;
        private readonly IMapper _mapper;
        private readonly UserManager<Account> _userManager;
        private readonly SignInManager<Account> _signInManager;
        public AccountService(
            IOptions<AuthOptions> authOptions,
            IMapper mapper,
            UserManager<Account> userManager,
            SignInManager<Account> signInManager)
        {
            _options = authOptions.Value;
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        public async Task<LoginResponseDto> Register(string email, string password, string firstName, string lastName)
        {

            Account user = new Account { Email = email, UserName = email, FirstName = firstName, LastName = lastName };


            var oldUser = await _userManager.FindByEmailAsync(email);


            if ( oldUser == null)
            {

                var result = await _userManager.CreateAsync(user, password);
                if (result == IdentityResult.Success)
                {
                    await _signInManager.SignInAsync(user, false);
                    return new LoginResponseDto() { success = true, payload = new LoginMessageSuccess() };
                }
            }

            return new LoginResponseDto() { success = false, payload = new LoginMessageError() { message = "Email is already in use" } };

        }
        public async Task<LoginResponseDto> Login(string email, string password)
        {
            var user = await _userManager.FindByEmailAsync(email);

            if (user != null)
            {
                if (user.IsActive)
                {
                    var result = await _signInManager.PasswordSignInAsync(user, password, false, false);
                    if (result.Succeeded)
                    {
                        var token = GenerateJWT(user);
                        return new LoginResponseDto() { success = true, payload = new LoginMessageSuccess() { access_token = token } };
                    }
                    return new LoginResponseDto() { success = false, payload = new LoginMessageError() { message = "Wrong password" } };
                }
                return new LoginResponseDto() { success = false, payload = new LoginMessageError() { message = "This user deleted" } };
            }
            return new LoginResponseDto() { success = false, payload = new LoginMessageError() { message = "User not found" } };


            //var result = await _signInManager.CheckPasswordSignInAsync(result2, password, lockoutOnFailure: true);
        }

        public async Task<IdentityResult> ChangePassword(string id, string password, string newPassword)
        {
            var user = await _userManager.FindByIdAsync(id);
            user.IsActive = true;
            var res = await _userManager.ChangePasswordAsync(user, password, newPassword);
            return res;
        }

        private string GenerateJWT(Account user)
        {
            _options.Issuer = "ahtuServer";
            _options.Audience = "resourceServer";
            _options.Secret = "secretKey123456789";
            _options.TokenLifetime = 36000;
            var securityKey = _options.GetSymmetricSecurityKey();
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new List<Claim>() {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim("Roles", user.Roles.ToString())
            };
            var token = new JwtSecurityToken(_options.Issuer,
                _options.Audience,
                claims,
                expires: DateTime.Now.AddSeconds(_options.TokenLifetime),
                signingCredentials: credentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<Account> GetUser(string id)
        {

            var res = await _userManager.FindByIdAsync(id);
            return res;
        }

        public async Task<List<Account>> GetAll()
        {
            var res = await _userManager.Users.ToListAsync();
            var result =  res.Where(x => x.IsActive == true).ToList(); ;
            return (List<Account>)result;
        }

        public async Task Activate(string id)
        {
            var res = await _userManager.FindByIdAsync(id);
            res.IsActive = true;
            var result = await _userManager.UpdateAsync(res);
        }


        public async Task DeActivate(string id)
        {
            var res = await _userManager.FindByIdAsync(id);
            res.IsActive = false;
            var result = await _userManager.UpdateAsync(res);
        }

        public async Task RoleChange(string id, int roles)
        {
             var res = await _userManager.FindByIdAsync(id);
             res.Roles = (Role)roles;
             var result = await _userManager.UpdateAsync(res);
        }

        public async Task<IdentityResult> ChangeName(string id, string FirstName, string LastName)
        {
            var user = await _userManager.FindByIdAsync(id);
            user.LastName = LastName;
            user.FirstName = FirstName;
            var result = await _userManager.UpdateAsync(user);
            return result;
        }

        public async Task<Account> GetUserOfToken(string token)
        {
          var userId = TokenRes(token);
            var res = await _userManager.FindByIdAsync(userId);
            return res;
        }
        public string TokenRes(string jwtToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadToken(jwtToken) as JwtSecurityToken;
            var tokenClaimsIdentity = new ClaimsIdentity(token.Claims);
            Lookup<string, string> claimsLookup = (Lookup<string, string>)tokenClaimsIdentity.Claims.ToLookup(p => p.Type, p => p.Value);
            string userId = "";
            foreach (var lookupItem in claimsLookup)
            {
                var key = lookupItem.Key;
                if (key == "sub")
                {
                    userId = lookupItem.FirstOrDefault();
                    return userId;
                }
            }return null;
        }

        public async Task<List<Account>> GetData(GetDataDto getDataDto)
        {
            var users = await _userManager.Users
                .Where(x => x.IsActive)
                .Skip(getDataDto.PageIndex * getDataDto.PageSize)
                .Take(getDataDto.PageSize)
                .ToListAsync();
            return users;
        }
        public async Task<int> GetLenghData()
        {

            var res = _userManager.Users
                .Where(x => x.IsActive)
                .Count();
            return res;
        }

        public async Task<IdentityResult> EditUser(Account user)
        {
            //var oldUser1 = await _userManager.FindByIdAsync(user.Id);
            var oldUser = await _userManager.FindByNameAsync(user.Email);
            //if (oldUser.Email != user.Email)
            //{
            //    oldUser.Email = user.Email;
                
            //}
            if (oldUser.FirstName != user.FirstName)
            {
                oldUser.FirstName = user.FirstName;

            }
            if (oldUser.FirstName != user.FirstName)
            {
                oldUser.FirstName = user.FirstName;
               
            }
            var result = await _userManager.UpdateAsync(oldUser);
            return result;
        }
    }
}
