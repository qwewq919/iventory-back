﻿using AutoMapper;
using iventoryExample.Entities;
using IventoryExample.Database.Repositories.Interfaces;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Room;
using IventoryExample.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Services.Implementations
{
    public class RoomService : IRoomService
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IMapper _mapper;
        public RoomService(
            IRoomRepository roomRepository,
            IMapper mapper)
        {
            _roomRepository = roomRepository;
            _mapper = mapper;
        }

        public async Task Add(CreateRoomDto item)
        {
            var entity = new Room(item.Name);
            _roomRepository.Add(entity);
            await _roomRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<GetRoomDto>> GetAll()
        {
            var data = await _roomRepository.GetAll();
            var allData = data.Where(b => b.IsActive);
            return _mapper.Map<IEnumerable<GetRoomDto>>(allData);
        }
        public async Task<GetRoomDto> GetById(Guid id)
        {
            var data = await _roomRepository.GetById(id);
            return _mapper.Map<GetRoomDto>(data);
        }

        

        public async Task Update(UpdateRoomDto item)
        {
            var entity = await _roomRepository.GetById(item.Id);

            if (entity == null)
            {
                throw new Exception($"Room with id {item.Id} was not found!");
            }

            _mapper.Map(item, entity);

            _roomRepository.Update(entity);
            await _roomRepository.SaveChangesAsync();
        }

        public async Task Activate(Guid id)
        {
            var entity = await _roomRepository.GetById(id);
            entity.IsActive = true;
            _roomRepository.Update(entity);
            await _roomRepository.SaveChangesAsync();
        }


        public async Task DeActivate(Guid id)
        {
            var entity = await _roomRepository.GetById(id);
            entity.IsActive = false;
            _roomRepository.Update(entity);
            await _roomRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<GetRoomDto>> GetData(GetDataDto getDataDto)
        {
            var rooms = await _roomRepository.GetData(getDataDto.PageIndex, getDataDto.PageSize);
            var allData = rooms.Where(b => b.IsActive);
            return _mapper.Map<IEnumerable<GetRoomDto>>(allData);
        }


        public async Task<int> GetLenghData()
        {
            var res = await _roomRepository.GetAllActive();
            var size = res.Count();
            return size;
        }
    }

}
