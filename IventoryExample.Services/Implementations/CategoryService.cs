﻿using AutoMapper;
using iventoryExample.Entities;
using IventoryExample.Database.Repositories.Interfaces;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Category;
using IventoryExample.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;
        public CategoryService(
            ICategoryRepository categoryRepository,
            IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task Add(CreateCategoryDto item)
        {
            var entity = new Category(item.Name);
            _categoryRepository.Add(entity);
            await _categoryRepository.SaveChangesAsync();
        }

        public async Task DeActivate(Guid id)
        {
            var entity = await _categoryRepository.GetById(id);

            entity.IsActive = false;

            _categoryRepository.Update(entity);
            await _categoryRepository.SaveChangesAsync();
        }

        public async Task Activate(Guid id)
        {
            var entity = await _categoryRepository.GetById(id);

            entity.IsActive = true;

            _categoryRepository.Update(entity);
            await _categoryRepository.SaveChangesAsync();
        }

        public async Task Update(UpdateCategoryDto item)
        {
            //var entity = new Category(item.Name);
            var entity = await _categoryRepository.GetById(item.Id);
            item.IsActivate = true;
            if(entity == null)
            {
                throw new Exception($"Category with id {item.Id} was not found!");
            }

            _mapper.Map(item, entity);

            _categoryRepository.Update(entity);
            await _categoryRepository.SaveChangesAsync();
        }

        public async Task<GetCategoryDto> GetById(Guid id)
        {
            var data = await _categoryRepository.GetById(id);
            return _mapper.Map<GetCategoryDto>(data);
        }
        
        public async Task <IEnumerable<GetCategoryDto>> GetAll()
        {
            var data = await _categoryRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<GetCategoryDto>>(data);
        }

        public async Task<IEnumerable<GetCategoryDto>> GetData(GetDataDto getDataDto)
        {
            var rooms = await _categoryRepository.GetData(getDataDto.PageIndex, getDataDto.PageSize);

            return _mapper.Map<IEnumerable<GetCategoryDto>>(rooms);
        }


        public async Task<int> GetLenghData()
        {
            var res = await _categoryRepository.GetAllAsync();
            var size = res.Count();
            return size;
        }

    }
}
