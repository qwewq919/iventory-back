﻿using AutoMapper;
using IventoryExample.Database.Repositories.Interfaces;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Logs;
using IventoryExample.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Services.Implementations
{
    public class LogsService: ILogsService
    {
        private readonly ILogsRepository _logsRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;
        public LogsService(
         ILogsRepository logsRepository,
         IMapper mapper,
         IHttpContextAccessor httpContextAccessor)
        {
            _logsRepository = logsRepository;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IEnumerable<GetLogsDto>> GetAll()
        {
            var data = await _logsRepository.GetLogsAsync();
            var res = _mapper.Map<IEnumerable<GetLogsDto>>(data);
            return (res);
        }
        public async Task<int> GetLenghData()
        {
            var items = await _logsRepository.GetAll();
            var size = items.Count();
            return size;
        }

        public async Task<IEnumerable<GetLogsDto>> GetData(GetDataDto getDataDto)
        {
            var items = await _logsRepository.GetLogsAsync();
            items = items.OrderByDescending(x => x.Date);
            var allData = items
                             .Skip(getDataDto.PageIndex * getDataDto.PageSize)
                             .Take(getDataDto.PageSize);

            return _mapper.Map<IEnumerable<GetLogsDto>>(allData);
        }

        public async Task<IEnumerable<GetLogsDto>> sort(SortLogsDto item)
        {
            
            var query = await GetAll();

            if (!string.IsNullOrWhiteSpace(item.items))
            {
                query = query.Where(x => x.Item?.Id.ToString() == item.items);
            }
            if (!string.IsNullOrWhiteSpace(item.user))
            {
                query = query.Where(x => x.Recipient?.Id.ToString() == item.user);
            }
            if (!string.IsNullOrWhiteSpace(item.Status.ToString()))
            {
                query =  query.Where(x => x.Status.ToString() == item.Status.ToString());
            }


            var result = query;
            return (IEnumerable<GetLogsDto>)result;
        }
    }

}
