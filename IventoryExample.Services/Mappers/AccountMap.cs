﻿using AutoMapper;
using iventoryExample.Entities;
using IventoryExample.Services.Dtos.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Mappers
{

    public class AccountMap : Profile
    {
        public AccountMap()
        {
            CreateMap<Account, GetAccountDto>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                .ForMember(x => x.Email, y => y.MapFrom(z => z.Email));
            CreateMap<UpdateAccountDto, Account>();
        }
    }
}
