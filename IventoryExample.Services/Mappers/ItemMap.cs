﻿using AutoMapper;
using iventoryExample.Entities;
using IventoryExample.Services.Dtos.Item;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Mappers
{
    public class ItemMap : Profile
    {
        public ItemMap()
        {
            CreateMap<Item, GetItemDto>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                .ForMember(x => x.Name, y => y.MapFrom(z => z.Name));
            CreateMap<UpdateItemDto, Item>();

            // будешь тут другие маппинги писать
            // на самом деле можно писать и просто
            // CreateMap<Category, GetCategoryDto>();
            // но в целях наглядности чуть подробнее расписал
            // если одинаково у обоих название поля - откуда и куда маппить
            // то он сам поймет как смаппить
            // здесь я мапплю потому что в DTO уже не нужно будет иметь поле IsActive
            // потому что это поле относится к уровню базы данных
            // оно нахуй не нужно на уровне сервисов
            // и тем более контроллера
            // а ещё тем более - с точки зрения фронта когда на него вернется моделька
            // так что вот так вот
            // если оба поля есть, но не хочешь вот какое - то маппить нагугли или позови
            // по авто мапперу миллион всяких объяснялок естьы
            // очень популярная залупа, помогает пиздец какие сложные структуры 
            // преобразовывать и не мучиться над логикой
        }
    }
}
