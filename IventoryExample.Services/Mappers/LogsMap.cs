﻿using AutoMapper;
using IventoryExample.Entities;
using IventoryExample.Services.Dtos.Logs;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Mappers
{
    public class LoogsMap : Profile
    {
        public LoogsMap()
        {
            CreateMap<Logs, GetLogsDto>();
        }
    }
}
