﻿using AutoMapper;
using iventoryExample.Entities;
using IventoryExample.Services.Dtos.Room;
using System;
using System.Collections.Generic;
using System.Text;

namespace IventoryExample.Services.Mappers
{
    public class RoomMap : Profile
    {
        public RoomMap()
        {
            CreateMap<Room, GetRoomDto>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                .ForMember(x => x.Name, y => y.MapFrom(z => z.Name));
            CreateMap<UpdateRoomDto, Room>();
        }
    }
}