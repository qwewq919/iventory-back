﻿using iventoryExample.Entities;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Room;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace IventoryExample.Services.Interfaces
{
    public interface IRoomService
    {
        Task Add(CreateRoomDto item);
        Task<GetRoomDto> GetById(Guid id);
        Task<IEnumerable<GetRoomDto>> GetAll();
        Task Update(UpdateRoomDto item);
        Task Activate(Guid id);
        Task DeActivate(Guid id);
        Task<IEnumerable<GetRoomDto>> GetData(GetDataDto getDataDto);
        Task<int> GetLenghData();

    }
}
