﻿using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Category;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Services.Interfaces
{
    public interface ICategoryService
    {
        Task<GetCategoryDto> GetById(Guid id);
        Task<IEnumerable<GetCategoryDto>> GetAll();
        Task Add(CreateCategoryDto item);
        Task Update(UpdateCategoryDto item);
        Task Activate(Guid id);
        Task DeActivate(Guid id);

        Task<IEnumerable<GetCategoryDto>> GetData(GetDataDto getDataDto);
        Task<int> GetLenghData();
    }
}
