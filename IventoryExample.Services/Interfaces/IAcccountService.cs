﻿using iventoryExample.Entities;
using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Account;
using IventoryExample.Services.Dtos.Auth;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace IventoryExample.Services.Interfaces
{
    public interface IAcccountService
    {
        //  Task<GetAccountDto> GetById(Guid id);
        //  Task<IEnumerable<GetAccountDto>> GetAll();
        ////  Task Add(CreateAccountDto item);
        //  Task Update(UpdateAccountDto item);
        //  Task Activate(Guid id);
        //  Task DeActivate(Guid id);
        Task<LoginResponseDto> Register(string email, string password, string firstName, string lastName);
        Task<LoginResponseDto> Login(string email, string password);
        Task<IdentityResult> ChangePassword(string id, string password, string newPassword);
        Task<Account> GetUser(string id);
        Task<List<Account>> GetAll();
        Task Activate(string id);
        Task DeActivate(string id);
        Task RoleChange(string email, int roles);
        Task<Account> GetUserOfToken(string id);
        Task<IdentityResult> ChangeName(string id, string FirstName, string LastName);
        Task<List<Account>> GetData(GetDataDto getDataDto);
        Task<int> GetLenghData();
        Task<IdentityResult> EditUser(Account user);


    }
}
