﻿using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Logs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IventoryExample.Services.Interfaces
{
    public interface ILogsService
    {
        Task<IEnumerable<GetLogsDto>> GetAll();
        Task<int> GetLenghData();
        Task<IEnumerable<GetLogsDto>> GetData(GetDataDto getDataDto);
        Task<IEnumerable<GetLogsDto>> sort(SortLogsDto item);
    }
}
