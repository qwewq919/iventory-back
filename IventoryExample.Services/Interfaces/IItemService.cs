﻿using IventoryExample.Services.Dtos;
using IventoryExample.Services.Dtos.Category;
using IventoryExample.Services.Dtos.Item;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;


namespace IventoryExample.Services.Interfaces
{
    public interface IItemService
    {
        Task Add(CreateItemDto item , string tokenProcessed );
        Task<GetItemDto> GetById(int id);
        Task<IEnumerable<GetItemDto>> GetAll();
        Task<IEnumerable<GetItemDto>> GetAllWithRooms();
        Task Update(UpdateItemDto item, string tokenProcesse);
        Task Activate(int id);
        Task DeActivate(int id ,string tokenProcessed);
        Task<IEnumerable<GetItemDto>> GetData(GetDataDto getDataDto);
        Task<int> GetLenghData();
        byte[] GetQR(string txtQRCode);
        Task<IEnumerable<GetItemDto>> sort(SortItemDto item);
        Task<string> Upload(IFormFile file, int id);
        Task<ImageStringDto> GetImgPath(int id);
        List<byte[]> ToPdf(IEnumerable<int> items);
        Task DeletUser(int id , string token);
        Task DeletCategorie(int id, string token);
        Task DeletRoom(int id, string token);
    }
}
